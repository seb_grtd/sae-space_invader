/**
 * Classe qui permet de représenter une barriere dans le jeu.
 */
public class Barriere {
    private int nbVies;
    private int positionX;
    private int positionY;

    private int compteurTours;
    private boolean estSupprimable;

    /**
     * Constructeur de la classe Barriere
     * @param positionX position en x de la barriere
     * @param positionY position en y de la barriere
     */
    public Barriere(int positionX, int positionY){
        this.nbVies = 30;
        this.positionX = positionX;
        this.positionY = positionY;
        this.compteurTours = 0;
        this.estSupprimable = false;
    }

    /**
     * Méthode qui permet de toucher la barriere (avec un tir).
     */
    public void touche(){
        this.nbVies--;
        if(this.nbVies == 0){
            this.compteurTours = 0;
        }
    }

    /**
     * Méthode qui permet de faire évoluer la barriere (utile uniquement pour faire l'animation lorsqu'elle se détruit).
     */
    public void evolue(){
        if(this.nbVies == 0){
            this.compteurTours++;
        }
        if (this.compteurTours > 80){
            this.estSupprimable = true;
        }
    }

    /**
     * Méthode qui permet de savoir si l'on peut supprimer la barrière de sa liste (si son animation de "mort" est terminée).
     */
    public boolean getEstSupprimable(){
        return this.estSupprimable;
    }

    /**
     * Méthode qui permet de savoir si la barriere est "vivante" ou non.
     * @return true si la barriere est "vivante", false sinon.
     */
    public boolean estVivant(){
        return this.nbVies > 0;
    }

    /**
     * Méthode qui permet de récupérer l'ensemble des chaines de caractères qui composent la barriere.
     * @return EnsembleChaines qui contient toutes les chaines de caractères qui composent la barriere.
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        if (this.nbVies == 0 && this.compteurTours % 18 >= 9){
            return e;
        }
        e.ajouteChaine(positionX, positionY+7, ""+(this.nbVies == 0 ? 1 : this.nbVies), TypeChaine.BARRIERE);
        return e;
    }

    /**
     * Méthode qui permet de savoir si la barriere contient les coordonnées d'un point (x, y).
     * @param x coordonnée x du point
     * @param y coordonnée y du point
     * @return true si la barriere contient le point (x, y), false sinon.
     */
    public boolean contient(int x, int y){
        return (x >= positionX+1 && x <= positionX + 13) && (y >= positionY && y <= positionY + 7);
    }

}
