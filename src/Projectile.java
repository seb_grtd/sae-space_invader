/**
 * Classe qui permet de représenter un projectile.
 */
public class Projectile {
    private double positionX;
    private double positionY;

    private boolean estProjectileVaisseau;

    /**
     * Constructeur de Projectile
     * @param positionX position en x du projectile
     * @param positionY position en y du projectile
     * @param estProjectileVaisseau booléen qui indique si le projectile est un projectile tiré par le vaisseau ou par un alien.
     */
    public Projectile(int positionX, int positionY, boolean estProjectileVaisseau) {
        this.positionX = (double) positionX;
        this.positionY = (double) positionY;
        this.estProjectileVaisseau = estProjectileVaisseau;
    }

    /**
     * Méthode qui permet d'obtenir l'ensemble de chaînes de caractères correspondant au projectile.
     * @return l'ensemble de chaînes de caractères correspondant au projectile.
     */
    public EnsembleChaines getEnsembleChaines() {
        EnsembleChaines e = new EnsembleChaines();
        e.ajouteChaine((int) positionX, (int) positionY+1, "█", estProjectileVaisseau ? TypeChaine.PROJECTILE_VAISSEAU_HAUT : TypeChaine.PROJECTILE_ALIEN_HAUT);
        e.ajouteChaine((int) positionX, (int) positionY, "█", estProjectileVaisseau ? TypeChaine.PROJECTILE_VAISSEAU_BAS : TypeChaine.PROJECTILE_ALIEN_BAS);
        return e;
    }

    /**
     * Méthode qui permet de faire évoluer le projectile.
     */
    public void evolue() {
        positionY += 0.8*(estProjectileVaisseau ? 1 : -1);
    }

    /**
     * Méthode qui permet de savoir la position en x du projectile.
     * @return la position en x du projectile.
     */
    public int getPositionX() {
        return (int) positionX;
    }

    /**
     * Méthode qui permet de savoir la position en y du projectile.
     * @return la position en y du projectile.
     */
    public int getPositionY() {
        return (int) positionY;
    }
}
