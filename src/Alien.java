/**
 * Classe qui permet de créer un alien, qui est un ennemi du joueur.
 */
public class Alien {
    private double positionX;
    private double positionY;

    private double vitesse;
    private double direction;
    private int compteurTours;
    private boolean estAnime;

    private boolean peutTirer;

    final int COOLDOWN_TIR =150;

    private int tempsEcoule;
    private int tempsEcouleDepuisMort;

    private boolean estVivant;
    private boolean supprimable;

    /**
     * Constructeur de la classe Alien
     * @param positionX position en x de l'alien
     * @param positionY position en y de l'alien
     * @param vitesse vitesse de l'alien
     */
    public Alien(int positionX, int positionY, double vitesse) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.vitesse = vitesse;
        this.compteurTours = 0;
        this.direction = 1;
        this.estAnime = false;
        this.tempsEcoule=0;
        this.tempsEcouleDepuisMort = 0;
        this.peutTirer=false;
        this.estVivant=true;
        this.supprimable = false;
    }

    /**
     * Méthode qui permet d'obtenir l'ensemble de chaînes de caractères correspondant à l'alien. s'adapte en fonction de si l'alien est en mode "animé" ou non.
    */
    public EnsembleChaines getEnsembleChaines() {
        EnsembleChaines e = new EnsembleChaines();
        if (this.estVivant){
            int typeChaine = TypeChaine.ALIEN;
            if (!this.estAnime){
                e.ajouteChaine((int) positionX, (int) positionY,   "     ▀▄   ▄▀     ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-1, "    ▄█▀███▀█▄    ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-2, "   █▀███████▀█   ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-3, "   █ █▀▀▀▀▀█ █   ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-4, "      ▀▀ ▀▀      ", typeChaine);
            }
            else{
                e.ajouteChaine((int) positionX, (int) positionY,   "   ▄ ▀▄   ▄▀ ▄   ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-1, "   █▄███████▄█   ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-2, "   ███▄███▄███   ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-3, "   ▀█████████▀   ", typeChaine);
                e.ajouteChaine((int) positionX, (int) positionY-4, "    ▄▀     ▀▄    ", typeChaine);
            }
        }
        else{
            int typeChaine = tempsEcouleDepuisMort < 15 ? TypeChaine.EXPLOSION_1 : tempsEcouleDepuisMort < 30 ? TypeChaine.EXPLOSION_2 : TypeChaine.EXPLOSION_1;
            e.ajouteChaine((int) positionX+3, (int) positionY+1,   "", typeChaine);
        }

        return e;
    }

    /**
     * Méthode qui permet de changer la direction de l'alien
     */
    public void changeDirection(){
        this.direction = -direction;
    }

    /**
     * Méthode qui permet de faire descendre l'alien
     */
    public void descendre(){
        positionY--;
    }

    /**
     * Méthode qui permet de faire avancer l'alien, et adapter son status(animation) en fonction du temps écoulé.
     */
    public void evolue() {
        positionX += vitesse*direction;
        if (compteurTours % 10 == 0)
            this.estAnime = !this.estAnime;
        compteurTours++;

        tempsEcoule++;
        if (tempsEcoule >= COOLDOWN_TIR){
            this.peutTirer=true;
        }
        if (!this.estVivant){
            tempsEcouleDepuisMort++;
            if (tempsEcouleDepuisMort >= 50){
                this.supprimable=true;
            }
        }
    }

    /**
     * Méthode qui permet d'obtenir la position en y de l'alien
     * @return position en y de l'alien
     */
    public double getPositionY() {
        return positionY;
    }

    /**
     * Méthode qui permet d'obtenir la position en x de l'alien
     * @return position en x de l'alien
     */
    public double getPositionX() {
        return positionX;
    }

    /**
     * Méthode qui permet de savoir si l'alien contient un autre point (x,y). Permet d'estimer si l'alien est touché par un tir.
     * Si l'alien est déjà mort et qu'il est en train d'être animé (explosion), on considère qu'il est n'est pas touché et la balle passera à travers.
     * @param x position en x du point
     * @param y position en y du point
     * @return true si l'alien contient le point et qu'il est vivant, false sinon.
     */
    public boolean contient(int x, int y) {
        return (x >= positionX + 3 && x <= positionX + 14 && y >= positionY - 4 && y <= positionY) && this.estVivant;
    }

    /**
     * Méthode qui permet de savoir si l'alien peut tirer ou non.
     * @return true si l'alien peut tirer, false sinon.
     */
    public boolean getPeutTirer(){return this.peutTirer && this.estVivant;}

    /**
     * Méthode qui permet de tuer l'alien
     */
    public void tuerAlien(){
        this.estVivant=false;
    }

    /**
     * Méthode qui permet de faire tirer l'alien
     */
    public void tir(){
        this.peutTirer = false;
        this.tempsEcoule = 0;
    }

    /**
     * Méthode qui permet de déterminer si l'on peut supprimer l'alien de la liste (une fois l'animation d'explosion terminée).
     * @return true si l'alien est supprimable, false sinon.
     */
    public boolean estSupprimable(){
        return this.supprimable;
    }
}
