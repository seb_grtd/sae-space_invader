import java.util.List;
import java.util.Iterator;

import java.util.ArrayList;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;

/**
 * Classe qui gère l'ensemble du jeu.
 */
public class GestionJeu {
    private int largeur;
    private int hauteur;
    private EnsembleChaines chaines;

    private Vaisseau vaisseau;

    private List<Alien> aliens;
    private List<Alien> aliensTouches;

    private List<Projectile> projectiles;
    private List<Projectile> projectilesQuiOntTouche;
    private List<Projectile> projectilesPerdus;
    private List<Projectile> projectilesDetruits;

    private List <Projectile> projectilesAlien;
    private boolean tirPossible;

    private Score score;
    private boolean estPerdue;

    private int nbVies;

    private TimerLancement timer;

    private int multiplicateurProbabiliteTirAlien;

    private double vitesseAlien;

    private int niveau;
    private int flashChaines;

    private boolean menuOuvert;
    private int choixMenu;

    private boolean musiqueJoue;

    private Media mediaMusique = new Media(new File("./sounds/musique.mp3").toURI().toString());
    private MediaPlayer musique = new MediaPlayer(mediaMusique);

    private Media mediaMenuSelect = new Media(new File("./sounds/menu_select.mp3").toURI().toString());
    private MediaPlayer menuSelect = new MediaPlayer(mediaMenuSelect);

    private Media mediaMenuHover = new Media(new File("./sounds/menu_hover.mp3").toURI().toString());
    private MediaPlayer menuHover = new MediaPlayer(mediaMenuHover);

    private Media mediaTir = new Media(new File("./sounds/shoot.mp3").toURI().toString());
    private MediaPlayer tir = new MediaPlayer(mediaTir);

    private Media mediaExplosion = new Media(new File("./sounds/explosion.mp3").toURI().toString());
    private MediaPlayer explosion = new MediaPlayer(mediaExplosion);

    private Media mediaDeath = new Media(new File("./sounds/death.mp3").toURI().toString());
    private MediaPlayer death = new MediaPlayer(mediaDeath);

    private Media mediaEnnemyShoot = new Media(new File("./sounds/ennemy_shooting.mp3").toURI().toString());
    private MediaPlayer ennemyShoot = new MediaPlayer(mediaEnnemyShoot);

    private Media mediaLevelUp = new Media(new File("./sounds/level_up.mp3").toURI().toString());
    private MediaPlayer levelUp = new MediaPlayer(mediaLevelUp);

    private Media mediaAlien1 = new Media(new File("./sounds/alien_1.wav").toURI().toString());
    private MediaPlayer alien1 = new MediaPlayer(mediaAlien1);

    private Media mediaAlien2 = new Media(new File("./sounds/alien_2.wav").toURI().toString());
    private MediaPlayer alien2 = new MediaPlayer(mediaAlien2);

    private Media mediaAlien3 = new Media(new File("./sounds/alien_3.wav").toURI().toString());
    private MediaPlayer alien3 = new MediaPlayer(mediaAlien3);

    private Media mediaAlien4 = new Media(new File("./sounds/alien_4.wav").toURI().toString());
    private MediaPlayer alien4 = new MediaPlayer(mediaAlien4);

    private int etatEvolutionAliens;

    private boolean sonActive;
    private boolean partieEnCoursQuandMenu;
    private boolean menuDifficulteOuvert;

    private int difficulte;

    private List<Barriere> barrieres;

    /**
     * Constructeur de la classe GestionJeu
     */
    public GestionJeu(){
        this.largeur = 160;
        this.hauteur = 60;
        this.score = new Score();

        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(this.largeur/2-5);

        this.projectiles = new ArrayList<Projectile>();
        this.projectilesQuiOntTouche = new ArrayList<>();
        this.projectilesPerdus = new ArrayList<>();
        this.projectilesDetruits = new ArrayList<>();

        this.aliens = new ArrayList<Alien>();
        this.aliensTouches = new ArrayList<>();
        this.projectilesAlien = new ArrayList<>();
        this.multiplicateurProbabiliteTirAlien = 400;
        this.vitesseAlien = 0.1;

        this.tirPossible = true;
        this.estPerdue = false;
        this.nbVies = 3;
        this.niveau=1;
        this.flashChaines = -1;

        this.timer = new TimerLancement(this.largeur/2-5,this.hauteur/2);

        this.menuOuvert = true;
        this.musiqueJoue = false;
        this.choixMenu = 0;

        this.etatEvolutionAliens =0;
        this.sonActive = true;

        this.partieEnCoursQuandMenu = false;
        this.menuDifficulteOuvert = false;

        this.difficulte = 1;

        this.barrieres = new ArrayList<>();

        for (int i = 0; i < 4; i++){
            this.barrieres.add(new Barriere(15+i*40, 10));
        }

        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 8; j++){
                this.aliens.add(new Alien(18 * j+7, hauteur - (7 * (i+1)) -3, vitesseAlien));
            }
        }
    }

    /**
     * Méthode qui permet de passer au prochain niveau. Elle réinitalise des variables et augmente la difficulté.
     */
    public void prochainNiveau(){
        this.vaisseau = new Vaisseau(this.largeur/2-5);

        this.projectiles = new ArrayList<Projectile>();

        this.projectilesAlien = new ArrayList<>();

        this.tirPossible = true;
        this.estPerdue = false;
        if (this.nbVies < 3)
            this.nbVies++;
        this.score.ajoute(this.niveau*1000);
        this.niveau++;
        this.vitesseAlien = this.vitesseAlien*1.5;
        this.flashChaines = -1;
        this.etatEvolutionAliens = 0;

        this.timer = new TimerLancement(this.largeur/2-5,this.hauteur/2);
        this.multiplicateurProbabiliteTirAlien = (int)(this.multiplicateurProbabiliteTirAlien*0.9);

        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 8; j++){
                this.aliens.add(new Alien(18 * j+7, hauteur - (7 * (i+1)) -3, vitesseAlien));
            }
        }
    }

    /**
     * Méthode qui permet de réinitialiser les variables pour rejouer une partie.
     */
    public void rejouer(){
        this.vaisseau = new Vaisseau(this.largeur/2-5);

        this.projectiles = new ArrayList<Projectile>();
        this.projectilesQuiOntTouche = new ArrayList<>();
        this.projectilesPerdus = new ArrayList<>();
        this.projectilesDetruits = new ArrayList<>();

        this.aliens = new ArrayList<Alien>();
        this.aliensTouches = new ArrayList<>();
        this.projectilesAlien = new ArrayList<>();
        this.multiplicateurProbabiliteTirAlien = 400;
        this.vitesseAlien = 0.1;

        this.tirPossible = true;
        this.estPerdue = false;
        this.nbVies = 3;
        this.niveau=1;
        this.flashChaines = -1;

        this.etatEvolutionAliens =0;

        this.score.reset();
        this.timer = new TimerLancement(this.largeur/2-5,this.hauteur/2);

        this.barrieres = new ArrayList<>();

        for (int i = 0; i < 4; i++){
            this.barrieres.add(new Barriere(15+i*40, 10));
        }

        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 8; j++){
                this.aliens.add(new Alien(18 * j+7, hauteur - (7 * (i+1)) -3, vitesseAlien));
            }
        }
    }

    /**
     * Méthode qui permet de gérer l'appui sur la touche de gauche (s'adapte en fonction de si l'on est dans le menu ou en pleine partie).
     */
    public void toucheGauche(){
        if (this.menuOuvert && this.menuDifficulteOuvert){
            this.menuDifficulteOuvert = false;
        }
        else{
            if(this.vaisseau.getPositionX() > 0 && this.timer.getEstTermine())
            this.vaisseau.deplace(-1);
        }
    }

    /**
     * Méthode qui permet de gérer l'appui sur la touche de droite (s'adapte en fonction de si l'on est dans le menu ou en pleine partie).
     */
    public void toucheDroite(){
        if (this.menuOuvert){
            menuSelect.stop();
            menuSelect.play();
            if (this.menuDifficulteOuvert){
                if (!this.partieEnCoursQuandMenu){
                    this.difficulte = (this.difficulte+1)%3;
                    this.changeDifficulte(difficulte);
                }
            }
            else{
                if (this.partieEnCoursQuandMenu){
                    switch (choixMenu){
                        case 0:
                            this.menuOuvert = false;
                            break;
                        case 1:
                            this.rejouer();
                            this.menuOuvert = false;
                            break;
                        case 2:
                            this.menuDifficulteOuvert = true;
                            break;
                        case 3:
                            toggleSon();
                            break;
                        case 4:
                            System.exit(0);
                            break;
                    }
                }
                else{
                    switch (choixMenu){
                        case 0:
                            if (this.nbVies == 0){
                                this.rejouer();
                            }
                            this.menuOuvert = false;
                            break;
                        case 1:
                            this.menuDifficulteOuvert = true;
                            break;
                        case 2:
                            toggleSon();
                            break;
                        case 3:
                            System.exit(0);
                            break;
                    }
                }
            }      
            return;
        }
        if(this.vaisseau.getPositionX() < this.largeur-8 && this.timer.getEstTermine())
            this.vaisseau.deplace(1);
    }

    /**
     * Méthode qui permet de gérer l'appui sur la touche espace. Elle permet de faire tirer le vaisseau si possible.
     */
    public void toucheEspace(){
        if (tirPossible && this.timer.getEstTermine()){
            tir.stop();
            tir.play();
            this.projectiles.add(new Projectile(this.vaisseau.positionCanon(), 7, true));
            tirPossible = false;
        }
    }

    /**
     * Méthode qui permet d'obtenir l'ensemble des chaînes de caractères qui composent le jeu.
     * @return l'ensemble des chaînes de caractères qui composent le jeu.
     */
    public EnsembleChaines getChaines(){
        return this.chaines;
    }

    /**
     * Méthode qui permet d'obtenir la largeur de la fenêtre.
     * @return la largeur de la fenêtre.
     */
    public int getLargeur(){
        return largeur;
    }

    /**
     * Méthode qui permet d'obtenir la hauteur de la fenêtre.
     * @return la hauteur de la fenêtre.
     */
    public int getHauteur(){
        return hauteur;
    }

    /**
     * permet de savoir si le jeu est gagné (le niveau est terminé). 
     * @return true si le jeu est gagné, false sinon.
     */
    public boolean estGagne(){
        return aliens.size() == 0;
    }

    /**
     * Permet de gérer la touche qui permet de quitter (Q).
     */
    public void toucheQuitter(){
        if (this.nbVies == 0 || estPerdue)
            System.exit(0);
    }

    /**
     * Permet de gérer la touche qui permet de rejouer (R).
     */
    public void toucheRejouer(){
        if (this.nbVies == 0 || estPerdue)
            this.rejouer();
    }

    /**
     * Permet de gérer la touche du bas lorsque le menu est ouvert.
     */
    public void toucheBas(){
        if (this.menuOuvert && ((this.partieEnCoursQuandMenu && choixMenu < 4) || choixMenu < 3)){
            choixMenu++;
            menuHover.stop();
            menuHover.play();
        }
    }

    /**
     * Permet de gérer la touche du haut lorsque le menu est ouvert.
     */
    public void toucheHaut(){
        if (this.menuOuvert && choixMenu > 0){
            choixMenu--;
            menuHover.stop();
            menuHover.play();
        }
    }

    /**
     * Permet de mettre le son en muet s'il ne l'est pas et inversement.
     */
    public void toggleSon(){
        int volume = this.sonActive ? 0 : 1;
        musique.setVolume(volume);
        menuHover.setVolume(volume);
        menuSelect.setVolume(volume);
        tir.setVolume(volume);
        explosion.setVolume(volume);
        death.setVolume(volume);
        ennemyShoot.setVolume(volume);
        levelUp.setVolume(volume);
        alien1.setVolume(volume);
        alien2.setVolume(volume);
        alien3.setVolume(volume);
        alien4.setVolume(volume);
        timer.toggleMute();
        this.sonActive = !this.sonActive;
    }

    /**
     * Permet de gérer la touche qui permet de mettre le jeu en pause (Echap) en revenant dans le menu.
     * Le menu s'adapte alors en fonction de si la partie est terminée ou non.
     */
    public void toucheEchap(){
        if (this.partieEnCoursQuandMenu || this.nbVies == 0 || this.estPerdue){
            if (!this.menuOuvert){
                this.choixMenu = 0;
                this.menuDifficulteOuvert = false;
                this.menuOuvert = true;
                this.musiqueJoue =false;
            }
            else {
                this.menuOuvert = false;
            }
        }
    }

    /**
     * Permet de changer la difficulté du jeu.
     * @param d entier correspondant à la difficulté à laquelle on veut passer (0 -> Facile, 1 -> Moyen, 3-> Expert).
     */
    public void changeDifficulte(int d){
        if (d == 0){
            this.multiplicateurProbabiliteTirAlien = 450;
            this.vitesseAlien = 0.05;
        }
        else if ( d == 1 ){
            this.multiplicateurProbabiliteTirAlien = 400;
            this.vitesseAlien = 0.1;
        }
        else{
            this.multiplicateurProbabiliteTirAlien = 350;
            this.vitesseAlien = 0.2;
        }
    }

    /**
     * Méthode permettant de jouer un tour de jeu.
     */
    public void jouerUnTour(){

        // réinitialisation des châines à chaque tour
        this.chaines = new EnsembleChaines();

        // gestion du menu
        if (this.menuOuvert){
            this.chaines.ajouteChaine(this.largeur/2-45, this.hauteur/2+15, "Space Invader", TypeChaine.TEXTE_TITRE);
            if (this.menuDifficulteOuvert){

                this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2+5, ">Difficulte: " + (this.difficulte == 0 ? "Facile" : this.difficulte == 1 ? "Moyen" : "Expert") , TypeChaine.TEXTE_MENU_SELECTIONNE);
                this.chaines.ajouteChaine(this.largeur/2-55, this.partieEnCoursQuandMenu ? 7 : 13, "DROITE : Changer de difficultée GAUCHE : retour", TypeChaine.TEXTE_INDICATION);
            }
            else{
                if (this.partieEnCoursQuandMenu){
                    this.chaines.ajouteChaine(this.largeur/2-35, choixMenu == 0 ? this.hauteur/2+5 : choixMenu == 1 ? this.hauteur/2 : choixMenu == 2 ? this.hauteur/2-5 : choixMenu == 3 ? this.hauteur/2-10 : this.hauteur/2-15, ">", TypeChaine.TEXTE_MENU_SELECTIONNE);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2+5, "Reprendre la partie", choixMenu == 0 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2, "Recommencer la partie", choixMenu == 1 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2-5,"Difficulté", choixMenu == 2 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2-10,sonActive ? "Son activé" : "Son désactivé", choixMenu == 3 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2-15,"Quitter", choixMenu == 4 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                }
                else{
                    this.chaines.ajouteChaine(this.largeur/2-35, choixMenu == 0 ? this.hauteur/2+5 : choixMenu == 1 ? this.hauteur/2 : choixMenu == 2 ? this.hauteur/2-5 : this.hauteur/2-10, ">", TypeChaine.TEXTE_MENU_SELECTIONNE);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2+5, "Commencer la partie", choixMenu == 0 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2,"Difficulté", choixMenu == 1 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2-5,sonActive ? "Son activé" : "Son désactivé", choixMenu == 2 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-30, this.hauteur/2-10,"Quitter", choixMenu == 3 ? TypeChaine.TEXTE_MENU_SELECTIONNE : TypeChaine.TEXTE_MENU);
                    this.chaines.ajouteChaine(this.largeur/2-47, this.partieEnCoursQuandMenu ? 7 : 13, "HAUT/BAS : séléctionner  DROITE : accéder", TypeChaine.TEXTE_INDICATION);
                }
            }
            this.chaines.ajouteChaine(this.largeur/2-25, 2, "Par Sébastien Gratade", TypeChaine.TEXTE_WATERMARK);
            if (!this.musiqueJoue){
                musique.setCycleCount(MediaPlayer.INDEFINITE);
                musique.play();
                this.musiqueJoue =true;
            }
            return;
        }   

        // permet de faire des actions une fois que le menu est quitté
        this.partieEnCoursQuandMenu = true;
        musique.stop();

        // ajoute les textes du haut de l'écran (vies, score, niveau)
        this.chaines.ajouteChaine(this.largeur/2-10, this.hauteur-5, "Score: "+score.getScore(), TypeChaine.TEXTE_SCORE);
        this.chaines.ajouteChaine(5, this.hauteur-5, "Vies: ", TypeChaine.TEXTE_VIES);
        this.chaines.ajouteChaine(25, this.hauteur-2, "", this.nbVies > 0 ? TypeChaine.COEUR : TypeChaine.COEUR_VIDE);
        this.chaines.ajouteChaine(25+10, this.hauteur-2, "", this.nbVies > 1 ? TypeChaine.COEUR : TypeChaine.COEUR_VIDE);
        this.chaines.ajouteChaine(25+10*2, this.hauteur-2, "", this.nbVies > 2 ? TypeChaine.COEUR : TypeChaine.COEUR_VIDE);
        this.chaines.ajouteChaine(this.largeur-37, this.hauteur-5, "Niveau: "+ this.niveau, TypeChaine.TEXTE_SCORE);

        // permet de faire "flasher" certaines chaines de caractères à l'écran (vaisseau/projectiles/aliens/barrières) pour donner un effet de clignotement.
        if (flashChaines >= 0 && flashChaines <=80){
            if (flashChaines%18 >= 9){
                this.chaines.union(vaisseau.getEnsembleChaines());
                for (Projectile p : projectilesAlien){
                    this.chaines.union(p.getEnsembleChaines());
                }
                for(Alien a: aliens){
                    this.chaines.union(a.getEnsembleChaines());
                }
                for(Barriere b : barrieres){
                    this.chaines.union(b.getEnsembleChaines());
                }
            }
            flashChaines++;
            return;
        }

        // Permet de gérer l'affichage pendant que le timer est en cours.
        if (!this.timer.getEstTermine()){
            this.projectilesAlien = new ArrayList<>();
            this.chaines.union(this.timer.getEnsembleChaines());
            this.chaines.union(vaisseau.getEnsembleChaines());
            for (Alien a : this.aliens){
                this.chaines.union(a.getEnsembleChaines());
            }
            for (Barriere barriere : this.barrieres){
                this.chaines.union(barriere.getEnsembleChaines());
            }
            this.timer.evolue();
            return;
        }

        // Permet de gérer le jeu si l'on gagne un niveau.
        if (estGagne()){
            levelUp.stop();
            levelUp.play();
            this.prochainNiveau();
            return;
        }

        // Permet de gérer le jeu si l'on perd (si on n'a plus de vie ou si les aliens sont descendus au niveau des défenses).
        // on affiche alors un écran de fin de partie résumant le score, le nombre d'aliens touchés, de projectiles perdus, de projectiles détruits et le niveau atteint.
        if (this.nbVies == 0 || estPerdue){
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2)+7, "   _____                         ____                 _ ", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2)+6, "  / ____|                       / __ \\               | |", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2)+5, " | |  __  __ _ _ __ ___   ___  | |  | |_   _____ _ __| |", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2)+4, " | | |_ |/ _` | '_ ` _ \\ / _ \\ | |  | \\ \\ / / _ \\ '__| |", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2)+3, " | |__| | (_| | | | | | |  __/ | |__| |\\ V /  __/ |  |_|", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2)+2, "  \\_____|\\__,_|_| |_| |_|\\___|  \\____/  \\_/ \\___|_|  (_)", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2)+1, "                                           ", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28, (hauteur/2), "                                           ", TypeChaine.TEXTE_ASCIIART);
            this.chaines.ajouteChaine(this.largeur/2-28+10, (hauteur/2)-2, "Votre score: " + score.getScore(), TypeChaine.TEXTE_FIN_PARTIE);
            this.chaines.ajouteChaine(this.largeur/2-28+7, (hauteur/2)-4, "Aliens touchés: " + aliensTouches.size(), TypeChaine.TEXTE_FIN_PARTIE);
            this.chaines.ajouteChaine(this.largeur/2-28+2, (hauteur/2)-6, "Projectiles perdus: " + projectilesPerdus.size(), TypeChaine.TEXTE_FIN_PARTIE);
            this.chaines.ajouteChaine(this.largeur/2-28+2, (hauteur/2)-8, "Projectiles detruits: " + projectilesDetruits.size(), TypeChaine.TEXTE_FIN_PARTIE);
            this.chaines.ajouteChaine(this.largeur/2-28+17, (hauteur/2)-10, "Niveau: " + this.niveau, TypeChaine.TEXTE_FIN_PARTIE);
            
            this.partieEnCoursQuandMenu = false;
            this.chaines.ajouteChaine(this.largeur/2-56, 13, "Rejouer (R) Quitter (Q) Menu (ESC)", TypeChaine.TEXTE_REJOUER_QUITTER);
            return;
        }

        // ajoute un au score
        this.score.ajoute(1);

        // Permet de gérer les projectiles aliés (ceux tirés par le vaisseau).
        // ici on vérifie si le projectile est sorti de l'écran, si oui on l'ajoute à la liste des projectiles perdus.
        // Sinon on vérifie si le projectile touche un alien, si oui on l'ajoute à la liste des projectiles qui ont touché un alien.
        // Sinon on vérifie si le projectile touche une barrière, si oui on l'ajoute à la liste des projectiles qui ont touché une barrière.
        // Si le projectile a touché l'un des deux, on ne les affiche pas et on les ajoute dans la liste de projectiles qui ont touché pour les supprimer de la liste plus tard.
        for(Projectile p : this.projectiles){
            p.evolue();
            if (p.getPositionY() > hauteur){
                this.projectilesPerdus.add(p);
                continue;
            }
            boolean aTouche = false;
            for (Alien a : this.aliens){
                if (a.contient(p.getPositionX(), p.getPositionY())){
                    explosion.stop();
                    explosion.play();
                    a.tuerAlien();
                    this.aliensTouches.add(a);

                    this.projectilesQuiOntTouche.add(p);
                    this.score.ajoute(100);
                    this.tirPossible = true;
                    aTouche = true;

                    break;
                }
            }
            for (Barriere b : this.barrieres){
                if (b.contient(p.getPositionX(), p.getPositionY())){
                    if (b.estVivant()){
                        b.touche();
                        this.score.ajoute(-50);
                        this.tirPossible = true;
                        this.projectilesQuiOntTouche.add(p);
                        aTouche = true;
                        break;
                    }
                }
            }
            if (!aTouche){
                this.chaines.union(p.getEnsembleChaines());
            }
        }

        // Permet de supprimer de la liste des projectiles alliés, ceux qui ont déjà touché.
        for(Projectile p: this.projectilesQuiOntTouche){
            this.projectiles.remove(p);
        }

        // Permet de supprimer de la liste des projectiles alliés, ceux qui sont sortis de l'écran.
        for (Projectile p: this.projectilesPerdus){
            if(this.projectiles.remove(p))
            {
                score.ajoute(-100);
                this.tirPossible = true;
            }
        }


        // permet de gérer aliens.
        // ici on vérifie si un alien est supprimable(animation terminée), si oui on le supprime de la liste des aliens sans générer d'erreurs avec Iterator.
        // Sinon on vérifie si au moins un alien sort des bords gauche ou droits de l'écran, si oui on change la direction de tous aliens et on les fait descendre.
        // (pour l'instant on ne stocke dans une variable que si l'on doit faire changer de direction les aliens/ les faire descendre dans la prochaine boucle.
        // pour éviter qu'ils ne changent plusieurs fois de direction dans le même tour de boucle) 
        // Si les aliens ne sortent pas de l'écran et qu'une condition aléatoire et validée, on fait changer leur direction pour les rendre plus imprévisibles.
        // Si un alien atteint le bas de l'écran, la partie est perdue.
        // Si un alien peut tirer, on vérifie si une condition aléatoire est validée, si oui il tire.
        boolean changerDirectionAliens = false;
        boolean faireDescendreAliens = false;

        Iterator<Alien> aliens = this.aliens.iterator();
        while (aliens.hasNext()){
            Alien a = aliens.next();
            if (a.estSupprimable() == true){
                aliens.remove();
                continue;
            }

            if (a.getPositionX() <= 0 || a.getPositionX() >= this.largeur-13)  {
                changerDirectionAliens = true;
                faireDescendreAliens = true;
            }
            else{
                if ((int)(Math.random() * 500) == 0){
                    changerDirectionAliens = true;
                }
            }
            if(a.getPositionY() == 18)
            {
                this.estPerdue = true;
                return;
            }
            if (a.getPeutTirer()) {
                if ((int)(Math.random() * this.multiplicateurProbabiliteTirAlien) == 0) {
                    a.tir();
                    ennemyShoot.stop();
                    ennemyShoot.play();
                    this.projectilesAlien.add(new Projectile((int) a.getPositionX() + 9, (int) a.getPositionY() - 6, false));
                }
            }
        }

        // ici on effectue les mouvements des aliens qu'on a stocké dans les variables précédentes.
        // et on fait évoluer les aliens.
        for (Alien a : this.aliens){
            if (changerDirectionAliens) {
                a.changeDirection();
            }
            if (faireDescendreAliens){
                a.descendre();
            }
            a.evolue();
            this.chaines.union(a.getEnsembleChaines());
        }

        
        // permet de faire changer le rythme des effets de bruit d'alien en fonction de la vitesse des aliens.
        this.etatEvolutionAliens++;
            if (this.etatEvolutionAliens == (int)(25*(1-vitesseAlien))){
                alien1.stop();
                alien1.play();
            }
            if (this.etatEvolutionAliens == (int)(50*(1-vitesseAlien))){
                alien2.stop();
                alien2.play();
            }
            if (this.etatEvolutionAliens == (int)(75*(1-vitesseAlien))){
                alien3.stop();
                alien3.play();
            }
            if (this.etatEvolutionAliens == (int)(100*(1-vitesseAlien))){
                alien4.stop();
                alien4.play();
                this.etatEvolutionAliens = 0;
            }    

        // Permet de gérer les projectiles des aliens.
        // On fait évoluer le projectile.
        // On vérifie si le projectile touche le vaisseau, si oui on le supprime de la liste des projectiles et on fait perdre une vie au joueur.
        // Sinon on vérifie si le projectile touche un projectile allié, si oui on supprime les deux projectiles de la liste des projectiles et on ajoute des points au score.
        // Sinon on vérifie si le projectile touche une barrière, si oui on supprime le projectile de la liste des projectiles et on fait perdre de la vie à la barrière.
        // Si le projectile n'a rien touché, on l'affiche, sinon non.
        Iterator<Projectile> iteratorProjectilesAlien = this.projectilesAlien.iterator();
        while (iteratorProjectilesAlien.hasNext()){
            Projectile p = iteratorProjectilesAlien.next();
            p.evolue();
            if(vaisseau.contient(p.getPositionX(), p.getPositionY())) {
                death.stop();
                death.play();
                this.nbVies-=1;
                flashChaines = 0;
                this.projectiles = new ArrayList<>();
                if (this.nbVies > 0){
                    this.tirPossible = true;
                    this.timer.reset();
                }
            }
            boolean aToucheProjectile = false;
            for (Projectile pVaisseau : this.projectiles){
                if (pVaisseau.getPositionX() == p.getPositionX() && pVaisseau.getPositionY()+1 == p.getPositionY()){
                    aToucheProjectile = true;
                    this.projectiles.remove(pVaisseau);
                    iteratorProjectilesAlien.remove();
                    this.projectilesQuiOntTouche.add(pVaisseau);
                    this.projectilesDetruits.add(p);
                    this.score.ajoute(100);
                    this.tirPossible = true;
                    break;
                }
            }
            for (Barriere b : this.barrieres){
                if (b.contient(p.getPositionX(), p.getPositionY())){
                    if (b.estVivant()){
                        b.touche();
                        this.score.ajoute(-50);
                        iteratorProjectilesAlien.remove();
                        aToucheProjectile = true;
                        break;
                    }
                }
            }
            if (!aToucheProjectile)
                this.chaines.union(p.getEnsembleChaines());
        }

        // Permet de gérer les barrières.
        // On fait évoluer les barrières
        // Et on vérifie si une barrière est supprimable(animation terminée), si oui on la supprime de la liste des barrières sans générer d'erreurs avec Iterator.
        Iterator<Barriere> lesBarrieres = this.barrieres.iterator();
        while(lesBarrieres.hasNext()){
            Barriere barriere = lesBarrieres.next();
            barriere.evolue();
            if (barriere.getEstSupprimable()){
                lesBarrieres.remove();
                continue;
            }
            this.chaines.union(barriere.getEnsembleChaines());
        }

        // on fait évoluer le vaisseau (pour l'animer).        
        vaisseau.evolue();

        // on ajoute finalement le vaisseau dans l'ensemble de chaînes.
        this.chaines.union(vaisseau.getEnsembleChaines());
    }
}
