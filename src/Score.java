/**
 * Classe qui permet de gérer le score
 */
public class Score {
    public int score;

    /**
     * Constructeur de la classe Score
     */
    public Score() {
        this.score = 0;
    }
    
    /**
     * Méthode qui permet d'ajouter des points au score
     * @param points points à ajouter
     */
    public void ajoute(int points) {
        this.score += points;
    }

    /**
     * Méthode qui permet d'obtenir le score
     * @return le score
     */
    public int getScore() {
        return score-score%20;
    }

    /**
     * Méthode qui permet de réinitialiser le score
     */
    public void reset() {
        this.score = 0;
    }
}
