import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.LinearGradient;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Stop;


public class Executable extends Application {
    private Pane root;
    private Group caracteres;
    private GestionJeu gestionnaire;
    private int hauteurTexte;
    private int largeurCaractere;
    public static void main(String[] args) {
        launch(args);
    }

    private void afficherCaracteres(){

        // la police d'écriture PressStart2P que j'utilise en trois tailles large (LG), normale et petite (SM).
        Font retroFontLG = Font.loadFont("file:./font/PressStart2P.ttf", 45);
        Font retroFont = Font.loadFont("file:./font/PressStart2P.ttf", 20);
        Font retroFontSM = Font.loadFont("file:./font/PressStart2P.ttf", 15);
        
        caracteres.getChildren().clear();
        
        int hauteur = (int) root.getHeight();

        for( ChainePositionnee c : gestionnaire.getChaines().chaines)
        {
            Text t = new Text(c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
            t.setFont(Font.font ("Monospaced", 10));
            caracteres.getChildren().add(t);

            // on obtient le type de la chaîne actuelle.
            int typeChaine = c.getTypeChaine();

            // si la chaine est de type VAISSEAU, VAISSEAU_ANIME, COEUR, COEUR_VIDE, EXPLOSION_1, EXPLOSION_2 ou BARRIERE on l'associe à l'image correspondante.
            if(typeChaine == TypeChaine.VAISSEAU || typeChaine == TypeChaine.VAISSEAU_ANIME || typeChaine == TypeChaine.COEUR 
            || typeChaine == TypeChaine.COEUR_VIDE || typeChaine == TypeChaine.EXPLOSION_1 || typeChaine == TypeChaine.EXPLOSION_2
            || typeChaine == TypeChaine.BARRIERE){
                Image img = null;
                switch(typeChaine){
                    case TypeChaine.VAISSEAU:
                        img = new Image("file:./img/ship.png");
                        break;
                    case TypeChaine.VAISSEAU_ANIME:
                        img = new Image("file:./img/ship_animated.png");
                        break;
                    case TypeChaine.COEUR:
                        img = new Image("file:./img/heart.png");
                        break;
                    case TypeChaine.COEUR_VIDE:
                        img = new Image("file:./img/heart_empty.png");
                        break;
                    case TypeChaine.EXPLOSION_1:
                        img = new Image("file:./img/explosion1.png");
                        break;
                    case TypeChaine.EXPLOSION_2:
                        img = new Image("file:./img/explosion2.png");
                        break;
                    case TypeChaine.BARRIERE:
                        img = new Image("file:./img/barriere/barriere" + c.c + ".png");
                        break;
                }
                ImageView vaisseau = new ImageView(img);
                vaisseau.setX(c.x*largeurCaractere);
                vaisseau.setY(hauteur - c.y*hauteurTexte);
                vaisseau.setFitHeight(typeChaine == TypeChaine.COEUR || typeChaine == TypeChaine.COEUR_VIDE ? 40 :  typeChaine == TypeChaine.EXPLOSION_1 || typeChaine == TypeChaine.EXPLOSION_2 ? 70 : typeChaine ==TypeChaine.BARRIERE ? 80 :50);
                vaisseau.setFitWidth(typeChaine == TypeChaine.COEUR || typeChaine == TypeChaine.COEUR_VIDE ? 40 :  typeChaine == TypeChaine.EXPLOSION_1 || typeChaine == TypeChaine.EXPLOSION_2 ? 70 : typeChaine ==TypeChaine.BARRIERE ? 80 :50);
                caracteres.getChildren().add(vaisseau);
            }

            // on prépare trois LinearGradient pour les projectiles du vaisseau, des aliens et les aliens eux-mêmes.
            LinearGradient gradientProjectileVaisseau = new LinearGradient(0, 0, 0, 1, true, null, new Stop[] {
                new Stop(0, Color.rgb(67, 139, 175)),
                new Stop(0.5, Color.rgb(60, 69, 171)),
                new Stop(0.5, Color.rgb(21, 77, 122)),
                new Stop(1, Color.rgb(17, 5, 66))
            });

            LinearGradient gradientProjectileAlien = new LinearGradient(0, 0, 0, 1, true, null, new Stop[] {
                new Stop(0, Color.rgb(40,5,66)),
                new Stop(0.5, Color.rgb(122,21,80)),
                new Stop(0.5, Color.rgb(171,60,73)),
                new Stop(1, Color.rgb(175,96,67))
            });

            LinearGradient gradientAlien = new LinearGradient(0, 0, 1, 0, true, null, new Stop[] {
                new Stop(0, Color.rgb(254,67,66)),
                new Stop(1, Color.rgb(229,29,81))
            });

            // finalement on gère la couleur et la taille de la police des châines qui n'ont pas d'images leur correspondant.
            switch(c.getTypeChaine()){
                case TypeChaine.PROJECTILE_VAISSEAU_BAS:
                    t.setFill(gradientProjectileVaisseau);
                    break;
                case TypeChaine.PROJECTILE_VAISSEAU_HAUT:
                    t.setFill(Color.rgb(67, 139, 175));
                    break;
                case TypeChaine.PROJECTILE_ALIEN_BAS:
                    t.setFill(Color.rgb(175,96,67));
                    break;
                case TypeChaine.PROJECTILE_ALIEN_HAUT:
                    t.setFill(gradientProjectileAlien);
                    break;
                case TypeChaine.ALIEN:
                    t.setFill(gradientAlien);
                    break;
                case TypeChaine.TEXTE_TITRE:
                    t.setFill(gradientAlien);
                    t.setFont(retroFontLG);
                    break;
                case TypeChaine.TEXTE_ASCIIART:
                    t.setFill(Color.rgb(255,255,255,1));
                    break;
                case TypeChaine.TEXTE_SCORE:
                    t.setFill(Color.rgb(120,116,169));
                    t.setFont(retroFont);
                    break;
                case TypeChaine.TEXTE_VIES:
                    t.setFill(Color.rgb(120,116,169));
                    t.setFont(retroFont);
                    break;
                case TypeChaine.TEXTE_FIN_PARTIE:
                    t.setFill(Color.rgb(255,255,255,1));
                    t.setFont(retroFontSM);
                    break;
                case TypeChaine.TEXTE_REJOUER_QUITTER:
                    t.setFill(Color.rgb(73, 201, 43));
                    t.setFont(retroFont);
                    break;
                case TypeChaine.TEXTE_MENU:
                    t.setFill(Color.rgb(255,255,255,1));
                    t.setFont(retroFont);
                    break;
                case TypeChaine.TEXTE_MENU_SELECTIONNE:
                    t.setFill(Color.rgb(73, 201, 43));
                    t.setFont(retroFont);
                    break;
                case TypeChaine.TEXTE_INDICATION:
                    t.setFill(Color.rgb(255,255,255,1));
                    t.setFont(retroFontSM);
                    break;
                case TypeChaine.TEXTE_WATERMARK:
                    t.setFill(Color.rgb(255,255,255,0.5));
                    t.setFont(retroFontSM);
                    break;
                case TypeChaine.BARRIERE:
                    // j'utilise le texte de barrière pour récupérer sa "vie" dans l'executable, je ne veux donc pas l'afficher.
                    t.setFill(Color.rgb(0,0,0,0));
                    break;
            }
        }
    }

    private void lancerAnimation() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                    new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent actionEvent) {
                            gestionnaire.jouerUnTour();
                            afficherCaracteres();
                        }
                    }),
                new KeyFrame(Duration.seconds(0.025))
                );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }


    @Override
        public void start(Stage primaryStage) {
            primaryStage.setTitle("IUTO Space Invader");
            caracteres = new Group();
            root= new AnchorPane(caracteres);

            gestionnaire = new GestionJeu();
            Text t=new Text("█");
            t.setFont(Font.font("Monospaced",10));
            hauteurTexte =(int) t.getLayoutBounds().getHeight();
            largeurCaractere = (int) t.getLayoutBounds().getWidth();

            Scene scene = new Scene(root,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            
            // on rajoute un fond
            Image bg = new Image("file:./img/background.png");
            scene.setFill(new ImagePattern(bg));

            scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
                if(key.getCode()==KeyCode.LEFT)
                    gestionnaire.toucheGauche();
                if(key.getCode()==KeyCode.RIGHT)
                    gestionnaire.toucheDroite();
                if (key.getCode()==KeyCode.UP)
                    gestionnaire.toucheHaut();
                if(key.getCode()==KeyCode.DOWN)
                    gestionnaire.toucheBas();
                if(key.getCode()==KeyCode.SPACE)
                    gestionnaire.toucheEspace();
                if(key.getCode()==KeyCode.R)
                    gestionnaire.toucheRejouer();
                if(key.getCode()==KeyCode.Q)
                    gestionnaire.toucheQuitter();
                if (key.getCode()==KeyCode.M)
                    gestionnaire.toggleSon();
                if(key.getCode()==KeyCode.ESCAPE)
                    gestionnaire.toucheEchap();
            });
            
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
            lancerAnimation();

        }
}
