/**
* Classe qui permet de stocker les différents types de chaînes de caractères.
*/
public class TypeChaine {
    public static final int PROJECTILE_VAISSEAU_HAUT = 0;
    public static final int PROJECTILE_VAISSEAU_BAS = 1;
    public static final int PROJECTILE_ALIEN_HAUT = 2;
    public static final int PROJECTILE_ALIEN_BAS = 3;
    public static final int VAISSEAU = 4;
    public static final int VAISSEAU_ANIME = 5;
    public static final int ALIEN = 6;
    public static final int ALIEN_ANIME = 7;
    public static final int TEXTE_TITRE = 8;   
    public static final int TEXTE_ASCIIART = 9;
    public static final int TEXTE_SCORE = 10;
    public static final int TEXTE_VIES = 11;
    public static final int TEXTE_FIN_PARTIE = 12;
    public static final int TEXTE_REJOUER_QUITTER = 13;
    public static final int TEXTE_MENU=14;
    public static final int TEXTE_MENU_SELECTIONNE=15;
    public static final int TEXTE_INDICATION = 16;
    public static final int TEXTE_WATERMARK = 17;
    public static final int COEUR = 18;
    public static final int COEUR_VIDE = 19;
    public static final int EXPLOSION_1 = 20;
    public static final int EXPLOSION_2 = 21;
    public static final int BARRIERE = 22;
}
