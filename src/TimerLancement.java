import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;


/**
 * Classe qui gère le timer de (re)lancement du jeu.
 */
public class TimerLancement {
    private int compteurTours;

    private int positionY;
    private int positionX;

    private boolean estTermine;

    private Media mediaCountDown3 = new Media(new File("./sounds/countdown_3.mp3").toURI().toString());
    private MediaPlayer countDown3 = new MediaPlayer(mediaCountDown3);

    private Media mediaCountDown2 = new Media(new File("./sounds/countdown_2.mp3").toURI().toString());
    private MediaPlayer countDown2 = new MediaPlayer(mediaCountDown2);

    private Media mediaCountDown1 = new Media(new File("./sounds/countdown_1.mp3").toURI().toString());
    private MediaPlayer countDown1 = new MediaPlayer(mediaCountDown1);

    private boolean estMute;

    /**
     * Constructeur de la classe TimerLancement
     * @param positionX position en x du timer
     * @param positionY position en y du timer
     */
    public TimerLancement(int positionX, int positionY){
        this.compteurTours =0;
        this.estTermine = false;
        this.positionX = positionX;
        this.positionY = positionY;
        this.estMute = false;
    }

    /**
     * Méthode qui fait évoluer le timer.
     */
    public void evolue(){
        this.compteurTours++;
        if (compteurTours == 1){
            countDown3.stop();
            countDown3.play();
        }
        if (compteurTours == 34){
            countDown2.stop();
            countDown2.play();
        }
        if (compteurTours == 66){
            countDown1.stop();
            countDown1.play();
        }
        if (this.compteurTours == 100){
            estTermine = true;
        }
    }

    /**
     * Méthode qui permet de réinitialiser le timer.
     */
    public void reset(){
        this.compteurTours = 0;
        this.estTermine = false;
    }

    /**
     * Méthode qui permet de rendre muet le timer s'il ne l'est pas, et inversement.
     */
    public void toggleMute(){
        if (estMute){
            countDown1.setVolume(1);
            countDown2.setVolume(1);
            countDown3.setVolume(1);
        }
        else{
            countDown1.setVolume(0);
            countDown2.setVolume(0);
            countDown3.setVolume(0);
        }
        this.estMute=!this.estMute;
    }

    /**
     * Méthode qui permet de savoir si le timer est terminé.
     * @return true si le timer est terminé, false sinon.
     */
    public boolean getEstTermine(){return this.estTermine;}

    /**
     * Méthode qui permet de récupérer l'ensemble des chaines de caractères qui composent le timer.
     * @return l'ensemble des chaines de caractères qui composent le timer.
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        int typeChaine = TypeChaine.TEXTE_ASCIIART;
        if (compteurTours < 33){
            e.ajouteChaine(positionX, (int) positionY+3, "  ____  ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY+2, " |___ \\ ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY, "  |__ < ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY+1, "   __) |", typeChaine);
            e.ajouteChaine(positionX, (int) positionY-1, "  ___) |", typeChaine);
            e.ajouteChaine(positionX, (int) positionY-2, " |____/ ", typeChaine);
        }
        else if (compteurTours <= 66){
            e.ajouteChaine(positionX, (int) positionY+3, "  ___  ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY+2, " |__ \\ ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY+1, "    ) |", typeChaine);
            e.ajouteChaine(positionX, (int) positionY, "   / / ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY-1, "  / /_ ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY-2, " |____|", typeChaine);
        }
        else{
            e.ajouteChaine(positionX, (int) positionY+3, "  __ ", typeChaine);
            e.ajouteChaine(positionX, (int) positionY+2, " /_ |", typeChaine);
            e.ajouteChaine(positionX, (int) positionY+1, "  | |", typeChaine);
            e.ajouteChaine(positionX, (int) positionY, "  | |", typeChaine);
            e.ajouteChaine(positionX, (int) positionY-1, "  | |", typeChaine);
            e.ajouteChaine(positionX, (int) positionY-2, "  |_|", typeChaine);
        }
        return e;
    }

}
