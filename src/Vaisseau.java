/**
 * Classe qui permet de représenter un vaisseau.
 */
public class Vaisseau {
    private double posX;
    private final int posY = 6;
    private int typeChaine;
    private int compteurTours;

    /**
     * Constructeur de la classe Vaisseau
     * @param posX position en x du vaisseau
     */
    public Vaisseau(double posX) {
        this.posX = posX;
        this.typeChaine = TypeChaine.VAISSEAU;
        this.compteurTours = 0;
    }

    /**
     * Méthode qui permet de déplacer le vaisseau
     * @param dx déplacement en x du vaisseau. (si négatif il ira à gauche)
     */
    public void deplace(double dx){
        this.posX += dx;
    }

    /**
     * Méthode qui permet de faire évoluer le vaisseau(l'animer).
     */
    public void evolue(){
        this.compteurTours++;
        if (compteurTours % 10 == 0) {
            if (typeChaine == TypeChaine.VAISSEAU) {
                typeChaine = TypeChaine.VAISSEAU_ANIME;
            } else {
                typeChaine = TypeChaine.VAISSEAU;
            }
        }
    }

    /**
     * Méthode qui permet de savoir la position en x du vaisseau.
     * @return la position en x du vaisseau.
     */
    public double getPositionX(){
        return this.posX;
    }

    /**
     * Méthode qui permet d'obtenir l'ensemble de chaînes de caractères correspondant au vaisseau.
     * @return l'ensemble de chaînes de caractères correspondant au vaisseau.
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        e.ajouteChaine((int)posX, posY, "        ", typeChaine);
        return e;
    }

    /**
     * Méthode qui permet de savoir la position en x du canon du vaisseau. (pour savoir d'où partira le projectile)
     * @return la position en x du canon du vaisseau.
     */
    public int positionCanon(){
        return (int)posX + 4;
    }

    /**
     * Méthode qui permet de savoir si le vaisseau contient un point (x,y).
     * @param x position en x du point
     * @param y position en y du point
     * @return true si le vaisseau contient le point (x,y), false sinon.
     */
    public boolean contient(int x, int y) {
        return (x >= posX && x <= posX + 8 && y >= 0 && y <= posY);
    }

}
