import java.util.ArrayList;

/**
 * Classe qui gère l'ensemble des chaînes de caractères affichées à l'écran.
 */

public class EnsembleChaines {
    ArrayList<ChainePositionnee> chaines;
    /**
     * Constructeur de la classe EnsembleChaines
     */
    public EnsembleChaines(){chaines= new ArrayList<ChainePositionnee>(); }

    /**
     * Méthode qui permet d'ajouter une chaîne de caractères à l'ensemble de chaînes
     * @param x position en x de la chaîne
     * @param y position en y de la chaîne
     * @param c chaîne de caractères à ajouter
     * @param TypeChaine type de la chaîne à ajouter
     */
    public void ajouteChaine(int x, int y, String c, int TypeChaine){
        chaines.add(new ChainePositionnee(x,y,c, TypeChaine));}

    /**
     * Méthode qui fait l'union de l'ensemble de chaînes actuel avec un autre.
     * @param e ensemble de chaînes à ajouter à l'ensemble actuel
     */
    public void union(EnsembleChaines e){
        for(ChainePositionnee c : e.chaines)
            chaines.add(c);
    }
}
