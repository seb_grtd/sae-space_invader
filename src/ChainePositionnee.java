/**
 * Classe qui permet de stocker une chaîne de caractères avec sa position en x et en y
 */
public class ChainePositionnee{
    int x,y;
    String c;
    int typeChaine;

    /**
     * Constructeur de la classe ChainePositionnee
     * @param a position en x de la chaîne
     * @param b position en y de la chaîne
     * @param d chaîne de caractères à ajouter
     * @param typeChaine type de la chaîne à ajouter
     */
    public ChainePositionnee(int a,int b, String d, int typeChaine){x=a; y=b; c=d; this.typeChaine = typeChaine;}

    /**
     * Méthode qui permet de connaître le type de la chaine;
     * @return le type de la chaine
     */
    public int getTypeChaine() {
        return typeChaine;
    }
}
